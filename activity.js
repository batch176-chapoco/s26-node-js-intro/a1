let http = require("http");

http.createServer(function (request, response) {
    console.log(request.url);
    if (request.url === "/") {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to B176 Booking System.");
    } else if (request.url === "/courses") {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to the Courses Page. View our Courses.")
    } else if (request.url === "/profile") {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to your Profile. View your Details.")
    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end("Resource not found.");
    }
}).listen(5000);
console.log("Server is running on localhost:5000");